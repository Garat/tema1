import React from 'react';

function BateriasLitio() {
    return (
        <div>
            <h3>Baterías eléctricas</h3>
            <p>
                Gracias a las nuevas tecnologías que afectan a las baterías,
                la autonomía de los coches eléctricos está aumentando a pasos agigantados.
            </p>
            <br />
        </div>
    );
}

export default BateriasLitio;