import React from 'react';

function PilasConvencionales() {
    return (
        <div>
            <h3>Pilas convencionales</h3>
            <p>
                A pesar del auge de las energias renovables,
                las pilas convencionales siguen utilizandose a diario.
            </p>
            <br />
        </div>
    );
}

export default PilasConvencionales;